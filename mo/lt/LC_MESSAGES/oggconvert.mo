��    ,      |  ;   �      �     �     �  
   �  
   �                 "         C     U     h     v     �  	   �     �     �     �     �  	   �     �  K        [     m  
   z     �     �     �     �  
   �     �     �            J        i     v     �     �  V   �     �     �            �  &     �     �     �     	  	   	  	   "	  
   ,	  (   7	     `	     u	     �	     �	  	   �	     �	     �	  %   �	     
     
  	   
  $   )
  E   N
     �
     �
     �
     �
  "   �
            
   ,     7     C     _     v  S   �     �     �     �     	  Y        r       U   �     �     	   )   '         "   $                         %   *      !           (                                                #                           
                &   +                ,                  %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-18 06:50+0000
Last-Translator: Julius Vitkauskas <zadintuvas@gmail.com>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% baigta, apie %s liko %i valandos %i minutės %i sekundės 1 valanda 1 minutė 1 sekundė <b><big>Konvertuojamas failas
</big></b> <b>Konvertavimas</b> <b>Paskirtis</b> <b>Šaltinis</b> <i>Konvertuojama „%s“</i> Išsamiau Visi failai Visi media failai Ar tikrai norite sustabdyti procesą? Garso failai Garso kokybė: Tada iki! Negalima išsaugoti į šį aplanką Pasirinkite kitą failo pavadinimą arba išsaugokite kitoje vietoje. Konvertavimas baigtas Failo formatas: Failo pavadinimas: Failas išsaugotas į „%s“. GStreamer klaida: preroll nepavyko Ogg
Matroska Ogg:
Matroska: OggConvert Pristabdyta Pristabdyta (%.1f%% baigta) Išsaugojimo aplankas: Pasirinkite aplanką Failas jau egzistuoja šioje vietoje (%s). Jį pakeitus bus perrašytas jo turinys. Theora
Dirac Video failai Video formatas: Video kokybė: Jūsų failai dabar yra konvertuojami į Ogg formatą. Tai gali užtrukti nemažai laiko. _Pristabdyti _Tęsti Launchpad Contributions:
  Julius Vitkauskas https://launchpad.net/~julius-vitkauskas nežinomas laikas  