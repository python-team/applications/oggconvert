��    +      t  ;   �      �     �     �  
   �  
   �     �     �       "        3     E     X     f     }  	   �     �     �     �     �  	   �     �  K   �     K     ]  
   j     u     �     �  
   �     �     �     �     �  J   �     9     F     R     `  V   o     �     �     �     �  �  �  "   �  	   �     �     �     �  
   �  	   �  &   �     &	     5	     B	     P	     g	     v	     �	     �	     �	     �	     �	  #   �	  Q   
     ^
     m
     }
  %   �
     �
     �
  
   �
     �
     �
            K   $     p     }  
   �     �  K   �  
   �     �  o        r     	   (             !   #               &          $   )                  '                                                "                          
                %   *                +                  %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-17 20:24+0000
Last-Translator: Lauri Niskanen <ape@ape3000.com>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% valmiina, noin %själjellä %i tuntia %i minuuttia %i sekuntia 1 tunti 1 minuutti 1 sekunti <b><big>Muunnetaan tiedosto
</big></b> <b>Muunnos</b> <b>Kohde</b> <b>Lähde</b> <i>Muunnetaan "%s"</i> Lisäasetukset Kaikki tiedostot Kaikki mediatiedostot Haluatko varmasti keskeyttää? Äänitiedostot Äänen laatu: Hei sitten! Ei voida tallentaa tähän kansioon Valitse toinen nimi tallennettavalle tiedostolle tai tallenna toiseen kohteeseen. Pakkaus valmis Tiedostotyyppi: Tiedostonimi: Tiedosto tallennettu kohteeseen "%s". Ogg
Matroska Ogg:
Matroska: OggConvert Pysäytetty Pysäytetty (%.1f%% valmiina) Tallenna kansioon: Valitse kansio Tiedosto on jo olemassa "%s":ssa. Korvaaminen ylikirjoittaa sen sisällön. Theora
Dirac Videotiedostot Kuvatyyppi Kuvan laatu: Mediatiedostoasi muunnetaan Ogg-muotoon. Tämä saattaa kestää pitkään. _Pysäytä _Jatka Launchpad Contributions:
  Lartza https://launchpad.net/~lartza
  Lauri Niskanen https://launchpad.net/~ape3000 tuntematon aika  