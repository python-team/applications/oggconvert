��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  &   �     	     	  	   	     (	     1	  	   :	  %   D	     j	     y	     �	     �	     �	     �	      �	  +   �	     
     "
     1
     9
  7   Y
     �
     �
     �
     �
  !   �
     �
       
        %     1     Q     b     r  B   {     �     �     �     �  L   �     E     Q  �   ^     D                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-17 20:24+0000
Last-Translator: Roman Horník <Unknown>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% hotovo, zbývá přibližně %s %i hodin %i minut %i sekund 1 hodina 1 minuta 1 sekunda <b><big>Převádím soubor
</big></b> <b>Převod</b> <b>Cíl</b> <b>Zdroj</b> <i>Převádím "%s"</i> Pokročilé Všechny soubory Všechny multimediální soubory Jste si jisti, že chcete převod zastavit? Audio soubory Kvalita zvuku: Nashle! Do této složky nelze ukládat Zvolte pro soubor jiný název, nebo jej uložte jinam. Převod dokončen Formát souboru: Název souboru: Soubor uložen do "%s". Chyba GStreameru: preroll selhalo Ogg
Matroska Ogg:
Matroska: OggConvert Pozastaveno Pozastaveno (dokončeno %.1f%%) Uložit složku: Vyberte složku Průběh Soubor již existuje v %s". Nahrazením bude přepsán jeho obsah. Theora
Dirac Video soubory Video formát: Kvalita obrazu: Váš soubor je právě převáděn do formátu Ogg. Může to trvat dlouho. _Pozastavit _Pokračovat Launchpad Contributions:
  Jakub Žáček https://launchpad.net/~dawon
  Kamil Páral https://launchpad.net/~kamil.paral
  Roman Horník https://launchpad.net/~roman.hornik
  Vojtěch Trefný https://launchpad.net/~vojtech.trefny neznámý čas  