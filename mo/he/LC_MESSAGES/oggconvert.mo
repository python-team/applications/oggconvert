��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  '   �     	     	     	     ,	     :	     H	  $   Z	     	     �	     �	     �	  
   �	     �	     �	  1   �	     /
     ?
     R
  ,   b
  N   �
     �
     �
     	          9     Y     f  
   u  
   �  "   �     �     �  
   �  _   �     D     Q     e     |  m   �  	     	     �        �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-05-06 12:30+0000
Last-Translator: el.il <Unknown>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% הסתיימו, כ-%s נותרו %i שעות %i דקות %i שניות שעה אחת דקה אחת שנייה אחת <b><big>ממיר קובץ
</big></b> <b>המרה</b> <b>יעד</b> <b>מקור</b> <i>"%s"ממיר את </i> מתקדם כל הקבצים כל קבצי המדיה האם אתה בטוח שברצונך לעצור? קבצי שמע איכות שמע: להתראות! לא ניתן לשמור לתיקייה זו בחר שם אחר לשמירת הקובץ, או שמור במיקום אחר. הקידוד הסתיים סוג קובץ: שם הקובץ: הקובץ נשמר אל "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert מושהה מושהה (%.1f%% הסתיימו) שמור תיקייה: בחר תיקייה מתחיל הקובץ כבר קיים ב-"%s". החלפתו תגרום למחיקת התוכן הקיים. Theora
Dirac קבצי וידאו פורמט וידאו: איכות ווידאו: כעת קובץ המדיה שלך מומר לפורמט Ogg. תהליך זה עשוי לערוך זמן רב. ה_שהה ה_משך Launchpad Contributions:
  Ddorda https://launchpad.net/~ddorda
  Hezy Amiel https://launchpad.net/~hezy
  Yaron https://launchpad.net/~sh-yaron
  el.il https://launchpad.net/~el-il זמן לא ידוע  