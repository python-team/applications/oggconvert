��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J     �  	   �     	     	     	     	     #	  $   )	     N	     \	     n	     |	     �	     �	     �	     �	     �	     �	     
  )   
  I   A
     �
     �
     �
     �
      �
     �
       
   $     /     6     T     c     w  =   �     �     �     �     �  `        i  
   {  f   �     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-02-20 14:51+0000
Last-Translator: oops <Unknown>
Language-Team: Korean <ko@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% 완료됨, 약 %s 남음 %i 시간 %i 분 %i 초 1시간 1 분 1 초 <b><big>파일 변환 중
</big></b> <b>변환</b> <b>보낼 곳</b> <b>소스</b> <i>변환중"%s"</i> 고급 설정(&A) 모든 파일 모든 미디어 파일 중단하시겠습니까? 음악 파일 오디오 음질: 그럼, 안녕히~! 이 폴더에 저장할 수 없습니다. 다른 이름으로 저장하거나, 다른 위치에 저장하십시오. 인코딩 완료 파일 형식: 파일 이름: "%s"로 파일이 저장됨. GStreamer 오류: preroll failed OGG:
마트료시카: OGG:
마트료시카: OggConvert 정지 일시 중지 (%.1f%% 완료) 저장 폴더: 폴더 선택하기 시작하는 중 "%s"에 파일이 존재합니다. 파일을 덮어씁니다. Theora
Dirac 동영상 파일 비디오 포맷: 비디오 화질: 미디어 파일이 OGG 형식으로 변환 중입니다. 시간이 좀 걸릴 수 있습니다. 잠시 멈춤(_P) 계속(_R) Launchpad Contributions:
  Elex https://launchpad.net/~mysticzizone
  oops https://launchpad.net/~oops 알 수 없는 시간  