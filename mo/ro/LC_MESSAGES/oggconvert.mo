��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  %   �     	  	   	  
   #	     .	     5	  
   =	  (   H	     q	     �	     �	  #   �	     �	     �	     �	  &   �	     "
     1
     A
     R
  S   r
     �
     �
     �
     �
  %        9     F  
   U  	   `     j     �     �     �  T   �          $     3     A  b   Q     �     �  O   �                      (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-24 22:28+0000
Last-Translator: Adrian Colomitchi <Unknown>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% complet, aproximativ %s rămas %i ore %i minute %i secunde 1 oră 1 minut 1 secundă <b><big>Fișier în conversie
</big></b> <b>Conversie</b> <b>Destinație</b> <b>Sursa</b> <i>Conversie a "%s" în progres</i> Avansat Toate fișierele Toate fișierele multimedia Ești sigur că dorești să oprești? Fişiere audio Calitate audio: Salutare atunci! Nu pot salva în acest director Alege un nume diferit pentru fișierul de salvat, sau salvează în altă locație. Encodare completă Format de fișier: Nume fişier: Fișier salvat în "%s". Eroare GStream: pre-derulare eșuată Ogg
Matroska Ogg:
Matroska: OggConvert Suspendat În suspensie (%.1f%% complet) Director de destinație: Selectează un director Pornesc Fișierul există deja în "%s". Înlocuirea lui va suprascrie conținutul acestuia. Theora
Dirac Fişiere video Format video: Calitate video: Fișierul dumneavoastră este convertit în format Ogg. S-ar putea să dureze un timp îndelungat. _Pauză _Reluare Launchpad Contributions:
  Adrian Colomitchi https://launchpad.net/~acolomitchi timp necunoscut  