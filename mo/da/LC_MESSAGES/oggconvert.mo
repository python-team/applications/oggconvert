��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  !   �     	     %	     1	     =	     D	     L	  "   U	     x	     �	     �	     �	  	   �	  
   �	     �	  &   �	  	   
     
  
   #
     .
  3   K
     
     �
     �
     �
  #   �
     �
     �
  
   �
               *     6     E  G   M     �     �     �     �  E   �       	     V   "     y                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-12-03 22:07+0000
Last-Translator: Jimmy Frydkær Jensen <thewolf40@mailme.dk>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% færdig, omkring %stilbage %i timer %i minutter %i sekunder 1 time 1 minut 1 sekund <b><big>Konverterer Fil
</big></b> <b>Konvertering</b> <b>Destination</b> <b>Kilde</b> <i>Konverterer</i> Avanceret Alle filer Alle Medie-filer Er du sikker på du ønsker at stoppe? Lyd Filer Lyd-kvalitet Farvel da! Kan ikke gemme i denne mappe Vælg et andet filnavn, eller gem i en anden mappe. Indkodning færdig Fil format: Filnavn: Fil gemt til "%s". GStreamer fejl: preroll mislykkedes Ogg
Matroska Ogg:
Matroska: OggConvert Holder pause Pauset (%.1f%% færdig) Gem-mappe : Vælg en mappe Starter Filen eksisterer allerede i "%s". Udskiftning vil overskrive indholdet. Theora
Dirac Video Filer Video Format: Video kvalitet Din medie-fil konverteres nu til Ogg format. Dette kan tage lang tid. _Pause _Genoptag Launchpad Contributions:
  Jimmy Frydkær Jensen https://launchpad.net/~jimmy-frydkaer tid ukendt  