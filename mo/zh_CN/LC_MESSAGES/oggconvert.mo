��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  "   �  	   	     )	     0	     7	     @	     F	  %   L	     r	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
     %
  -   A
     o
     |
     �
     �
  !   �
     �
     �
  
   �
  	   �
          %     6     F  G   S     �     �     �     �  Q   �  
   '  
   2  �   =     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-17 20:32+0000
Last-Translator: Tao Wei <weitao1979@gmail.com>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% 已完成， 大约%s剩余 %i 小时 %i 分 %i 秒 1 小时 1 分 1 秒 <b><big>正在转换文件
</big></b> <b>转换</b> <b>目的</b> <b>来源</b> <i>正在转换 "%s"</i> 高级设置 所有文件 所有多媒体文件 你确实希望停止？ 音频文件 音频质量： 再见！ 不能保存到此文件夹 选择不同的名称或位置保存文件。 编码完成 文件格式： 文件名： 保存文件到"%s"。 GStreamer 错误：preroll failed Ogg
Matroska Ogg：
Matroska： OggConvert 已暂停 暂停 （%.1f%% 已完成） 保存文件夹: 选择文件夹 正在启动 文件夹"%s" 中已有此文件。替换则会覆盖原来的内容。 Theora
Dirac 视频文件 视频格式： 视频质量： 你的多媒体文件正在转换成Ogg格式。这个过程需要一些时间。 暂停(_P) 继续(_R) Launchpad Contributions:
  Joo https://launchpad.net/~joo.tsao
  Tao Wei https://launchpad.net/~weitao1979
  luojie-dune https://launchpad.net/~luojie-dune 未知时间  