��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  8   �     2	     @	     N	     ^	     g	     v	  <   �	  6   �	     �	      
  (   6
  )   _
     �
  (   �
  I   �
          $  	   A  <   K  l   �  1   �     '     @  "   S  &   v     �     �  
   �     �  8   �           ;  !   W  U   y     �     �     �       �   (     �     �  �   �      �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 20:32+0000
Last-Translator: Michael Sinchenko <sinchenko@krvgarm.net>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% выполнено, около %sосталось %i часов %i минут %i секунд 1 час 1 минута 1 секунда <b><big>Конвертирование файла
</big></b> <b>Настройки преобразования</b> <b>Результат</b> <b>Исходный файл</b> <i>Преобразование "%s"</i> Расширенные настройки Все файлы Все мультимедиа файлы Вы уверены, что хотите прервать процесс? Аудио файлы Качество звука: Пока! Невозможно сохранить в эту папку Выбирите другое имя для файла или сохраните в другую папку. Конвертирование выполнено Формат файла: Имя файла: Файл сохранён в "%s". Ошибка GStreamer: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Приостановлено Приостановлено (%.1f%% выполнено) Сохранить в папку Выберите папку Запуск приложения Файл уже существует в "%s". Он будет перезаписан. Theora
Dirac Видео файлы Формат видео: Качество видео: Ваш файл в данный момент преобразуется в формат Ogg. Это может занять длительное время. _Пауза _Возобновить Launchpad Contributions:
  Alexander Kabakow https://launchpad.net/~alexzak
  Artem Urvanov https://launchpad.net/~artem62
  Michael Sinchenko https://launchpad.net/~sinchenko-krvgarm
  Xamuh https://launchpad.net/~stepan-skrjabin время неизвестно  