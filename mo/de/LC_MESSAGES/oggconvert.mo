��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  "   �  
   	  
   &	     1	     =	     I	     U	  $   b	     �	     �	     �	     �	  	   �	     �	     �	  )   �	     (
     5
     E
  %   V
  X   |
     �
     �
  
   �
        %        E     R  
   a  
   l     w     �     �     �  d   �     '     4     A     N  U   ^     �     �  )  �     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 20:34+0000
Last-Translator: Maximilian R. <max.reininghaus@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% fertig, ungefähr %s übrig %i Stunden %i Minuten %i Sekunden eine Stunde eine Minute eine Sekunde <b><big>Konvertiere Datei
</big></b> <b>Umwandlung</b> <b>Ziel</b> <b>Quelle</b> <i>"%s" wird konvertiert</i> Erweitert Alle Dateien Alle Multimediadateien Sind Sie sicher, dass Sie beenden wollen? Audiodateien Audioqualität: Auf Wiedersehen! Kann nicht in diesen Ordner speichern Bitte wählen Sie einen anderen Namen oder einen anderen Speicherort für die Datei aus. Kodierung komplett Dateiformat: Dateiname: Datei unter "%s" abgespeichert GStreamer-Fehler: preroll gescheitert Ogg
Matroska Ogg:
Matroska: OggConvert Angehalten Angehalten (%.1f%% komplett) Speicherort: Ordner auswählen Wird gestartet Diese Datei existiert bereits in "%s". Wenn Sie diese ersetzen, überschreiben Sie damit den Inhalt. Theora
Dirac Videodateien Videoformat: Videoqualität: Die Datei wird gerade in das Ogg-Format umgewandelt. Das kann eventuell lange dauern. _Pause _Fortsetzen Launchpad Contributions:
  Blackout https://launchpad.net/~coolx67
  Data https://launchpad.net/~ubuntuaddress
  Friedrich Weber https://launchpad.net/~fredreichbier
  Martin Schaaf https://launchpad.net/~mascha
  Maximilian R. https://launchpad.net/~max-r
  thmsslg https://launchpad.net/~thmsslg unbekannte Zeit  