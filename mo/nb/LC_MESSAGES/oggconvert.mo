��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J     �     	     "	     .	     :	     A	     J	  "   S	     v	     �	     �	     �	     �	  
   �	     �	  "   �	     �	     
     
     
  =   6
     t
  
   �
     �
     �
  +   �
     �
     �
     �
       !        -     :     H  G   P     �  
   �     �     �  C   �       	     �   !     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-01-24 23:38+0000
Last-Translator: Simen Heggestøyl <simen@simenh.com>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% ferdig, rundt %sleft %i timer %i minutter %i sekunder 1 time 1 minutt 1 sekund <b><big>Konverterer fil
</big></b> Konvertering <b>Mål</b> <b>Kilde</b> <i>Konverterer "%s"</i> Avansert Alle Filer Alle Mediafiler Er du sikker på at du vil stoppe? Lydfiler Lydkvalitet: Adjø! Kan ikke lagre i denne mappen Velg et annet navn for denne filen, eller lagre et annet sted Omkoding ferdig Filformat: Filnavn: Filen ble lagret i "%s". GStreamer feil: forhåndsutrullingen feilet Ogg
Matroska Ogg:
Matroska: OggOmformer Pause Satt på pause (%.1f%% completed) Lagre mappe: Velg en mappe Starter Filen finnes allerede i "%s". Å erstatte den vil overskrive innholdet. Theora
Dirac Videofiler Videoformat: Videokvalitet: Din mediafil blir konvertert til Ogg-format. Dette kan ta lang tid. _Pause _Fortsett Launchpad Contributions:
  Marstein https://launchpad.net/~clmar
  OttifantSir https://launchpad.net/~ottifantsirtarrant
  kingu https://launchpad.net/~comradekingu ukjent tid  