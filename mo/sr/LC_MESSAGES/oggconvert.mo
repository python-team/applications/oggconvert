��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  <   �     )	     5	     E	  	   W	     a	     n	  2   	     �	     �	     �	  %   �	      
     1
  .   G
  F   v
     �
     �
     �
  F     m   K  %   �     �     �  %     &   1     X     e     t  
   �  ,   �     �  +   �     	          �     �     �     �  p   �     f     r  �   �                      (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-24 10:42+0000
Last-Translator: Slobodan Paunović <Unknown>
Language-Team: Serbian <sr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% је завршено, преостало је %sleft %n сати %i минута %i секунда %n сат 1 минут 1 секунда <b><big>Конвертован фајл
</big></b> <b>Конверзија</b> <b>Одредиште</b> <b>Извор</b> <i>Конвертује се "%s"</i> Напредно Сви фајлови Сви аудио и видео фајлови Да ли си сигуран да желиш да зауставиш? Аудио фајлови Квалитет звука: Поздрав! Не могу да сачувам у овај директоријум Изабери друго име за фајл или га сачувај на другој локацији. Конверзија обављена Формат фајла: Име фајла: Фајл је сачуван у "%s". GStreamer грешка: preroll failed Ogg
Matroska Ogg:
Matroska: Ogg конвертор Пауза Паузирано (%.1f%% завршено) Директоријум: Изаберите директоријум Почетак Фајл већ постоји у "%s". Ако желите да га замените, игубићете стари фајл. Theora
Dirac Видео фајлови Формат видеа: Квалитет видеа: Твој фајл се тренутно конвертује у Ogg format. Ово може да потраје. _Пауза _Настави Launchpad Contributions:
  Slobodan Paunović https://launchpad.net/~slobodan-paunovic
  Vladimir Lazic https://launchpad.net/~vlazic непознато време  