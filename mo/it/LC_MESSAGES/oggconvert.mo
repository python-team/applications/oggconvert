��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  #   �     	  	    	  
   *	     5	     ;	  	   D	  #   N	     r	     �	     �	  %   �	     �	     �	     �	     �	  
   
     *
     :
  &   @
  Z   g
     �
     �
  
   �
     �
  !        '     4  
   C     N     W     t     �     �  V   �       
             *  g   :     �  	   �    �     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 20:34+0000
Last-Translator: Luca Falavigna <dktrkranz@ubuntu.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% completato, rimasti %s circa %i ore %i minuti %i secondi 1 ora 1 minuto 1 secondo <b><big>Conversione file
</big></b> <b>Conversione</b> <b>Destinazione</b> <b>Sorgente</b> <i>Conversione di «%s» in corso</i> Avanzate Tutti i file Tutti i file multimediali Si è certi di voler terminare? File audio Qualità audio: Ciao! Impossibile salvare in questa cartella Scegliere un nome diverso per il file da salvare, oppure salvare in una posizione diversa. Codifica completata Formato file: Nome file: File salvato in "%s". Errore GStreamer: preroll fallito Ogg
Matroska Ogg:
Matroska: OggConvert In pausa In pausa (%.1f%% completato) Salva nella cartella: Selezionare una cartella In avvio Il file esiste già in "%s". Se viene sostituito il suo contenuto verrà sovrascritto. Theora
Dirac File video Formato video: Qualità video: È in corso la conversione del file multimediale nel formato Ogg. Questo potrebbe richiedere del tempo. _Pausa _Riprendi Launchpad Contributions:
  Antonio Cono https://launchpad.net/~tblu
  Luca Falavigna https://launchpad.net/~dktrkranz
  Nicola Piovesan https://launchpad.net/~piovesannicola
  Sergio Zanchetta https://launchpad.net/~primes2h
  Tristan Brindle https://launchpad.net/~tristan-brindle durata sconosciuta  