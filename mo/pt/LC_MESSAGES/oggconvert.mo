��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  "   �     	  
   	     	     %	     ,	  	   5	  '   ?	     g	     y	     �	     �	  	   �	     �	     �	  "   �	     
     !
  
   3
  &   >
  L   e
     �
     �
     �
     �
  %        3     @  
   O     Z     b     j       	   �  Q   �     �     �          !  u   3     �     �  }   �     8                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 22:11+0000
Last-Translator: Tiago <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% completo, falta cerca de %s %i horas %i minutos %i segundos 1 hora 1 minuto 1 segundo <b><big>A Converter Ficheiro
</big></b> <b>Conversão</b> <b>Destino</b> <b>Origem</b> <i>A Converter "%s"</i> Avançado Todos os Ficheiros Todos os Ficheiros Multimédia Tem a certeza de que deseja parar? Ficheiros de Áudio Qualidade Áudio: Adeusinho! Não foi possível guardar nesta pasta Escolha um nome diferente para o ficheiro a guardar, ou guarde noutro local. Codificação completa Formato de Ficheiro: Nome de Ficheiro: Ficheiro guardado como "%s". Erro GStreamer: <i>preroll</i> falhou Ogg
Matroska Ogg:
Matroska: OggConvert Pausado Pausado Pasta de Gravação: Seleccione Uma Pasta A Iniciar O ficheiro já existe em "%s". Ao substituí-lo irá re-escrever o seu conteúdo. Theora
Dirac Ficheiros de Vídeo Formato Vídeo: Qualidade Vídeo: O seu ficheiro multimédia está a ser convertido para o formato Ogg. Esta operação poderá demorar bastante tempo. _Pausar _Retomar Launchpad Contributions:
  André Malafaya Baptista https://launchpad.net/~malafaya
  Tiago https://launchpad.net/~tiagosilva tempo desconhecido  