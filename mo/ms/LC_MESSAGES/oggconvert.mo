��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  #   �      	     	     	     	     	     &	  %   -	     S	     e	     v	     �	     �	  
   �	     �	  "   �	  
   �	     �	     �	  "   
  3   4
     h
     v
  
   �
     �
     �
     �
     �
  
   �
     �
     �
            	   !  I   +     u  
   �     �     �  P   �     �  	     k        x                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-02-24 14:59+0000
Last-Translator: Joey <Unknown>
Language-Team: Malay <ms@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% siap, lebih kurang %stinggal %i jam %i minit %i saat sejam seminit sesaat <b><big>Sedang Menukar
File</big></b> <b>Pertukaran</b> <b>Destinasi</b> <b>Sumber</b> <i>Sedang Tukar "%s"</i> Lanjutan Semua Fail Semua File Media Adakah anda pasti untuk berhenti ? File Audio Kualiti Audio: Selamat tinggal ! Tidak dapat disimpan ke folder ini Sila pilih nama yang lain atau ke folder yang lain. Encoding siap Format File: Nama File: File disimpan ke "%s". Ralat GStreamer: preroll gagal Ogg
Matroska Ogg:
Matroska: OggConvert Rehat Rehat (%.1f%% siap) Folder Simpan: Pilih Folder Memulakan File sudah wujud. Menggantikannya akan menyebabkan file sedia ada hilang. Theora
Dirac File Video Format Video: Kualiti Video: Media anda sedang ditukar ke format ogg. Ini akan mengambil masa yang agak lama. _Rehat _Teruskan Launchpad Contributions:
  Joey https://launchpad.net/~jakkalster
  alHakim https://launchpad.net/~nurhakim tidak tahu masa  