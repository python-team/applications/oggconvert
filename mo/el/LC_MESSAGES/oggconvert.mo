��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  C   �     :	     F	     T	     p	     y	     �	  4   �	     �	     �	     
  %   
  !   B
     d
  +   }
  L   �
     �
          '  K   <  �   �  4   *     _     z  7   �  &   �     �       
     
     ,   &  &   S  $   z     �  �   �     X     e          �  �   �     E     Q  O   c     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-25 00:26+0000
Last-Translator: Kainourgiakis Giorgos <kaingeo@yahoo.gr>
Language-Team: Greek <el@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% ολοκληρωμένο, περίπου %s απομένει %i ώρες %i λεπτά %i δευτερόλεπτα 1 ώρα 1 λεπτό 1 δευτερόλεπτο <b><big>Μετατροπή αρχείου
</big></b> <b>Μετατροπή</b> <b>Προορισμός</b> <b>Πηγή</b> <i>Μετατροπή του "%s"</i> Για προχωρημένους Όλα τα αρχεία Όλα τα αρχεία πολυμέσων Είστε σίγουροι ότι θέλετε να σταματήσετε; Αρχεία ήχου Ποιότητα ήχου: Αντίο τότε! Αδυναμία αποθήκευσης σε αυτόν τον φάκελο Επιλέξτε ένα διαφορετικό όνομα για το αρχείο ή αποθηκεύστε το σε διαφορετική τοποθεσία. Η κωδικοποίηση ολοκληρώθηκε Μορφή αρχείου: Όνομα αρχείου: Το αρχείο αποθηκεύτηκε στο "%s". Σφάλμα GStreamer: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Παύση Παύση (%.1f%% ολοκληρωμένο) Φάκελος αποθήκευσης: Επιλέξτε ένα φάκελο Γίνεται εκκίνηση Το αρχείο ήδη υπάρχει στο "%s". Αν το αποθηκεύσετε τα περιεχόμενα θα αντικατασταθούν. Theora
Dirac Αρχεία βίντεο Μορφή βίντεο: Ποιότητα βίντεο: Το αρχείο σας μετατρέπεται σε μορφή Ogg. Αυτό μπορεί να διαρκέσει αρκετό χρόνο. _Παύση _Συνέχεια Launchpad Contributions:
  Kainourgiakis Giorgos https://launchpad.net/~kaingeo άγνωστος χρόνος  