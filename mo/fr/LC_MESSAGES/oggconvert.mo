��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  #   �  	   		  
   	     	     *	     2	  	   ;	  (   E	     n	     �	     �	     �	     �	     �	     �	  %   �	     
     %
     6
  (   B
  E   k
     �
     �
     �
     �
  7        ?     N  
   ]     h     q     �     �  
   �  y   �     D     Q     a     w  `   �     �  
   �  �       �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 20:33+0000
Last-Translator: Steve Dodier <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% terminé, environ %s restant %i heures %i minutes %i secondes 1 heure 1 minute 1 seconde <b><big>Conversion du fichier
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Conversion de "%s"</i> Avancé Tous les Fichiers Tous les Fichiers Média Êtes-vous sûr de vouloir arrêter ? Fichiers Sonores Qualité Audio : Au revoir ! Impossible d'enregistrer dans ce dossier Choisissez un nom ou un répertoire différent pour l'enregistrement. Encodage terminé Format du fichier : Nom du Fichier : Fichier sauvegardé dans "%s". Erreur de GStreamer: Initialisation échouée (Preroll) Ogg:
Matroska: Ogg:
Matroska: OggConvert En pause En pause (%.1f%% accomplis) Dossier de Destination : Sélectionnez un Dossier Démarrage Le fichier nommé de la même manière existe déjà dans "%s". Le remplacer supprimera définitivement l'ancien fichier. Theora
Dirac Fichiers Vidéo Format de la Vidéo : Qualité Vidéo : Votre fichier est en cours de conversion dans le format Ogg. Cela peut prendre un certain temps. _Mettre en pause _Reprendre Launchpad Contributions:
  Commandant https://launchpad.net/~gabriel-oger
  Guillaume Lanquepin https://launchpad.net/~guyomel
  Lux1n https://launchpad.net/~lux1n
  Penegal https://launchpad.net/~penegal
  Pierre Slamich https://launchpad.net/~pierre-slamich
  Samuel Hym https://launchpad.net/~samuel-hym
  Steve Dodier https://launchpad.net/~sidi
  arno_b https://launchpad.net/~arno.b Temps inconnu  