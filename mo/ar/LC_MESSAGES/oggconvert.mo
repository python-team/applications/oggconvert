��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  +   �     
	      	     7	     N	     b	     x	  (   �	     �	     �	     �	     �	  
   
     
     -
  5   L
     �
     �
     �
  1   �
  \   �
     J     f     y     �  .   �     �     �  
   �               6     K  
   ]  r   h     �     �            t   2     �     �  @   �                      (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-12-02 10:38+0000
Last-Translator: MaXeR <Unknown>
Language-Team: Arabic <ar@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% مكتمل, حوالي %s متبقي % من الساعات %i من الدقائق %i من الثواني ساعة واحدة دقيقة واحدة ثانية واحدة <b><big>تحويل الملف
</big></b> <b>التحويل</b> <b>الوِجهة</b> <b>المصدر</b> <i>تحويل "%s"</i> متقدم كل الملفات كل ملفات الوسائط هل أنت متأكد أنك تريد التوقف؟ ملفات الصوت جودة الصوت: وداعاً! لا يمكن الحفظ في هذا المجلد إختر إسماً آخر لحفظ اللف, أو إحفظ الملف في مكان آخر. إكتمال التحويل نوع الملف: اسم الملف: الملف حُفظ في "%s". مشكلة في GStreamer: فشل في preroll Ogg
Matroska Ogg:
Matroska: OggConvert موقف مؤقتًا متوقف (%.1f%% مكتمل) مجلد الحفظ: اختر مجلد بداية هذا الملف موجود حالياً في "%s". إستبداله يعني الكتابة فوق محتواه. Theora
Dirac ملفات الفيديو نوع الفيديو: جودة الفيديو: ملف الوسائط يُحوّل حالياً إلى نمط Ogg. هذا ربما يأخذ وقتاً طويلاً. إيقاف مؤقت إستئناف Launchpad Contributions:
  MaXeR https://launchpad.net/~themaxer وقت غير معلوم  