��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  7   �     "	     .	     >	     P	     Y	     h	  ;   y	  !   �	  %   �	     �	  &   
     <
     U
  4   q
  L   �
     �
  !        /  @   7  q   x  (   �          .  -   F  )   t     �     �  
   �     �  +   �  %        '     C  �   X     �     �  !      $   "  �   G            �   4      �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-13 20:34+0000
Last-Translator: B.U.S.T.T.E.R. <Unknown>
Language-Team: Bulgarian <bg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% завършени, остават около %s %i часа %i минути %i секунди 1 час 1 минута 1 секунда <b><big>Преобразуване на файл
</big></b> <b>Преубразуване</b> <b>Местонахождение</b> <b>Източник</b> <i>Преобразуване "%s"</i> Допълнителни Всички файлове Всички мултимедийни файлове Сигурен(а) ли сте, че искате да прекратите? Аудио файлове Качество на звука: Чао! Запазването в папката е невъзможно Изберете друго име за файла или го запазете на различно място. Кодирането е завъшено Файлов формат: Име на файла: Файлът е запазен в(ъв) "%s". Грешка с GStreamer: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert На пауза На пауза (%.1f%% завършени) Папка за съхранение: Изберете Папка Стартиране Файлът вече съществува в(ъв) "%s".Замяната му ще презапише съдържанието. Theora
Dirac Видео файлове Формат на видеото: Качество на видеото Мултимедийния Ви файл в момента се преобразува във формат с разширение Ogg. Това може да отнеме известно време. _Пауза _Продължение Launchpad Contributions:
  B.U.S.T.T.E.R. https://launchpad.net/~bustter
  Emil Pavlov https://launchpad.net/~emil-p-pavlov
  Ivaylo Ivanov https://launchpad.net/~icefox неизвестно време  