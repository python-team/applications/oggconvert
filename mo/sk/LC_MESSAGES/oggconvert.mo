��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J     �  	   	  
   	  
   	     %	  	   .	  	   8	  $   B	     g	     x	     �	     �	     �	     �	     �	     �	     �	     
  
   
  +   !
  K   M
     �
     �
     �
     �
      �
            
   "     -  !   :     \     q     �  <   �     �     �     �     �  R        Y     `    n     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-17 21:24+0000
Last-Translator: Roman Horník <Unknown>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% hotovo, okolo %zostáva %i hodiny %i minúty %i sekundy 1 hodina 1 minúta 1 sekunda <b><big>Konverzia súboru
</big></b> <b>Konverzia</b> <b>Cieľ</b> <b>Zdroj</b> <i>Konverzia"%s"</i> Pokročilé Všetky súbory Multimediálne súbory Naozaj si prajete skončiť? Zvukové súbory Kvalita zvuku Dovidenia! Nie je možné ukladať do tohto priečinka Zvoľte iné meno pre ukladaný súbor, alebo zmeňte miesto pre uloženie. Prevod dokončený Formát súboru Názov súboru: Súbor uložený do "%s" Chyba GStreameru: preroll zlyhal Ogg
Matroska Ogg:
Matroska: OggConvert Pozastavené Pozastavené (%.1f%% dokončené) Uložiť priečinok: Vybrať priečinok Spúšťa sa Súbor už existuje v "%s". Nahradenie prepíše jeho obsah. Theora
Dirac Súbory videa Video formát: Kvalita videa Váš súbor je práve prevádzaný do formátu Ogg. Može to trvať dlhší čas. _Pauza _Pokračovať Launchpad Contributions:
  Alexander Yuriev https://launchpad.net/~cooler-email
  Jakub Kapuš https://launchpad.net/~kapo
  Miroslav Mereďa https://launchpad.net/~miro-me
  Miroslav Ďurian https://launchpad.net/~aasami
  Roman Horník https://launchpad.net/~roman.hornik Neznámy čas  