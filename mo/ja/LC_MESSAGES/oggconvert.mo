��    *      l  ;   �      �     �     �  
   �  
   �     �     �     �  "         #     5     H     V     m  	   v     �     �     �     �     �  K   �     1     C  
   P     [     o     |  
   �     �     �     �     �     �  J   �     +     8     D     R  V   a     �     �     �  �  �     y  	   �     �     �     �     �     �  7   �     �     		     	  $   +	     P	     ]	  $   v	  *   �	     �	     �	  0   �	  K   
     ]
  !   s
     �
  0   �
     �
     �
  
   �
                -     =     S  �   i     �               3  �   :     �  
   �  �   �     	   (                 #               &          $   )                  '   !                                            "                          
                %   *                                   %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2010-04-16 13:59+0000
Last-Translator: Hiroshi Tagawa <Unknown>
Language-Team: Japanese <ja@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% 完了、残り約 %s %i 時間 %i 分 %i 秒 1 時間 1 分 1 秒 <b><big>ファイルを変換しています
</big></b> <b>変換</b> <b>変換先</b> <b>ソース</b> <i>"%s" を変換しています</i> 詳細設定 すべてのファイル すべてのメディアファイル 本当に中止してもいいですか？ 音声ファイル 音質 このフォルダには保存できません。 ファイル名を変えるか、別の場所に保存してください。 エンコード完了 ファイルフォーマット： ファイル名： ファイルは "%s" に保存されました。 Ogg
Matroska Ogg:
Matroska: OggConvert 一時停止 一時停止中 (%.1f%% 完了) フォルダ： フォルダを選択 開始しています ファイルはすでに "%s" に存在しています。置き換えるとファイルの中身を上書きすることになります。 Theora
Dirac 動画ファイル ビデオフォーマット： 画質 メディアファイルを Ogg フォーマットに変換しています。これには時間がかかるかも知れません。 一時停止(_P) 再開(_R) Launchpad Contributions:
  Hiroshi Tagawa https://launchpad.net/~kuponuga
  tagawa https://launchpad.net/~a-launchpad-kyokodaniel-com 