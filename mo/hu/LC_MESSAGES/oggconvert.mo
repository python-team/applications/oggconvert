��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J     �     	     	     	     $	     +	     2	  '   ?	     g	     |	     �	     �	     �	     �	     �	     �	     �	     
     
  !   $
  =   F
     �
     �
  
   �
  "   �
  $   �
     �
               )     6     R     g  	   �  I   �     �     �     �       `        r     �  �   �     s                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-11-17 23:47+0000
Last-Translator: Muszela Balázs <Unknown>
Language-Team: Hungarian <hu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% Kész,  %shátravan %i óra %i perc %i másodperc 1 óra 1 perc 1 másodperc <b><big>Fájl konvertálása
</big></b> <b>Konvertálás</b> <b>Cél</b> <b>Forrás</b> <i>"%s" konvertálása</i> Haladó Minden fájl Minden média fájl Biztos meg akarja állítani? Hangfájlok Hangminőség: Akkor viszlát! Nem menthető ebbe a könyvtárba Válasszon más nevet a mentéshez, vagy más helyre mentsen. Kódolás befejezve Fájlformátom: Fájlnév: Fájl mentve a következőbe: "%s" GStreamer hiba: a preroll sikertelen Ogg
Matroska Ogg:
Matroska: Ogg konvertálás Megállítva Megállítva (%.1f%% kész) Mentési könyvtár: Válasszon egy könyvtárat Indítás A fájl már létezik: "%s". Ha lecseréli azzal felülírja a tartalmát Theora
Dirac Videofájlok Videoformátum: Videominőség: A média fájlok Ogg formátumba konvertálása folyamatban van. Ez hosszabb ideig is eltarthat. _Megállítás _Folytatás Launchpad Contributions:
  G. U. https://launchpad.net/~gergo86
  Gergely Szarka https://launchpad.net/~gszarka
  Muszela Balázs https://launchpad.net/~bazsi86-deactivatedaccount
  Pittmann Tamás https://launchpad.net/~zaivaldi ismeretlen idő  