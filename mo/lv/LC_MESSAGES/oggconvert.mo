��    -      �  =   �      �     �        
   	  
             &     /  "   8     [     m     �     �     �  	   �     �     �     �     �  	          K   '     s     �  
   �     �     �     �     �  
   �     �     �          &     6  J   B     �     �     �     �  V   �          !     )     <  �  J  )   	  
   D	     O	     [	     g	  	   p	  	   z	  "   �	     �	     �	     �	     �	     �	  
   �	     
  +   
     F
     R
     d
     q
  F   �
     �
     �
     �
     	  +         L     Y  
   h     s     |     �     �     �  E   �     	          "     2  U   D     �  
   �  L   �     �                 (                     *           "                 -   	   #   !           +   $                                           )           %                            
      &   '         ,              %.1f%% completed, about %sleft %i hours %i minutes %i seconds 1 hour 1 minute 1 second <b><big>Converting File
</big></b> <b>Conversion</b> <b>Destination</b> <b>Source</b> <i>Converting "%s"</i> Advanced All Files All Media Files Are you sure you wish to stop? Audio Files Audio Quality: Bye then! Cannot save to this folder Choose a different name for the save file, or save to a different location. Encoding complete File Format: File Name: File saved to "%s". GStreamer error: preroll failed Ogg
Matroska Ogg:
Matroska: OggConvert Paused Paused (%.1f%% completed) Save Folder: Select A Folder Starting up The file already exists in "%s". Replacing it will overwrite its contents. Theora
Dirac Video Files Video Format: Video Quality: Your media file is currently being converted to Ogg format. This may take a long time. _Pause _Resume translator-credits unknown time  Project-Id-Version: oggconvert
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-09-12 23:34+0000
PO-Revision-Date: 2009-12-24 05:30+0000
Last-Translator: Launchpad Translations Administrators <rosetta@launchpad.net>
Language-Team: Latviešu <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-05-23 15:24+0000
X-Generator: Launchpad (build Unknown)
 %.1f%% izpildīti, atlikušas apmēram %s %i stundas %i minūtes %i sekundes 1 stunda 1 minūte 1 sekunde <b><big>Konvertē failu
</big></b> <b>Konvertēšana</b> <b>Galamērķis</b> <b>Avots</b> <i>Konvertē "%s"</i> Papildus Visi faili Visi mediju faili Esat pārliecināts, ka vēlaties apturēt? Audio faili Audio kvalitāte: Nu tad atā! Šajā mapē nevar saglabāt Izvēlieties citu saglabāšanas faila vārdu, vai saglabājiet citur. Kodēšana izpildīta Faila formāts: Faila vārds: Fails saglabāts "%s". GStreamer kļūda: sagatavošana neizdevās Ogg
Matroska Ogg:
Matroska: OggConvert Pauzēts Pauzēts (%.1f%% izpildīti) Saglabāt mapē: Izvēlēties mapi Sākam Fails jau eksistē mapē "%s". Aizvietošana pārrakstīs tā saturu. Theora
Dirac Video faili Video formāts: Video kvalitāte: Jūsu medija fails tiek konvertēts uz Ogg formātu. Tas var aizņemt kādu laiciņu. _Pauze _Turpināt Launchpad Contributions:
  Tranzistors https://launchpad.net/~rudolfs-mazurs nezināms laiks  